from os import system, path
import speedtest

try:
    s = speedtest.Speedtest()

    # Bytes for conversion
    bytes_num = 1000000

    # Get download speed
    dws = round(s.download() / bytes_num, 2)

    # Get upload speed
    ups = round(s.upload() / bytes_num, 2)

    message = f'Bajada {dws} Mbps - Subida {ups} Mbps'

    system(f'notify-send -i {path.dirname(__file__)}/assets/dialog-information.png -t 10000 "Velocidad internet" "{message}"')
except Exception as e:
    system(f'notify-send -t 10000 "Velocidad internet" "Error: {e}"')